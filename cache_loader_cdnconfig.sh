#!/bin/bash

i=0

for file1 in cdnconfig-keys/*
do
  i=$((i+1))
  file=`echo $file1 | cut -d'/' -f 2 | cut -d'_' -f 1`
  cp cdnconfig-values/${file}_res cdnconfig-keys/
  python cache_loader_cdnconfig.py cdnconfig-keys/${file}
  rm -f cdnconfig-keys/${file}_res
done


