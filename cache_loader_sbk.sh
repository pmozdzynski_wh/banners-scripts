#!/bin/bash

i=0

for file1 in sbk-keys/*
do
  i=$((i+1))
  file=`echo $file1 | cut -d'/' -f 2 | cut -d'_' -f 1`
  cp sbk-values/${file}_res sbk-keys/
  python cache_loader_sbk.py sbk-keys/${file}
  rm -f sbk-keys/${file}_res
  #if [ $i -ge $1 ]
  #then
  #  exit
  #fi
done


