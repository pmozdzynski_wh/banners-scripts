#!/bin/bash

i=0

for file1 in banners-keys/*
do
  i=$((i+1))
  file=`echo $file1 | cut -d'/' -f 2 | cut -d'_' -f 1`
  cp banners-values/${file}_res banners-keys/
  python cache_loader_banners.py banners-keys/${file}
  rm -f banners-keys/${file}_res
done


